
#include "socket.h"
#include <string>
#include <string.h> 

class Server {
	
	public: 
	Server(char type, int ipv4_ipv6); 
	void bind(int port, int ipv4_ipv6);
	void listen(int length_queue);
	int accept(struct sockaddr_in* socket_client);
	int accept(struct sockaddr_in6* socket_client6); 
	void read(char* buf, int id_socket);    
	void write(char* buf, int client_id); 
	
		
	private: 
	//char* msg_from_client; 
	int type_server; 				//0 ipv4  1 ipv6
	Socket* server_socket; 
	//struct sockaddr_in* socket_client; 
	//struct sockaddr_in6* socket_client6; 
	//int client_id; 
	
	std::string answer_to_client = "mensaje recibido \n"; 

}; 
