
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include "socket.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/* 
   char tipo: el tipo de socket que quiere definir
      's' para "stream
      'd' para "datagram"
   bool ipv6: si queremos un socket para IPv6
 */
  
  
#define CLIENT_QUEUE_LEN 10  
 
Socket::Socket( char tipo, bool ipv6 ){									//ESTE

int id = 0; 
this->ipv6 = ipv6; 


if (ipv6) {
	switch(tipo) {
		case 's': 
		case 'S': 
			id = socket(AF_INET6, SOCK_STREAM, 0); 		//if ese id es 0, ocurrio 1 error. 
			//0 ó -1 ? 
			
		break; 
		
		case 'd':
		case 'D':
			id = socket(AF_INET6, SOCK_DGRAM, 0);
		break;	
	}
}
else {		//estamos en ipv4. 
	switch(tipo) {
		case 's':
		case 'S':
		
			id = socket(AF_INET, SOCK_STREAM, 0); 
		break; 
		
		case 'd':
		case 'D':
			id = socket(AF_INET, SOCK_DGRAM, 0); 
		break; 
		
	}
}

this->id = id; 
//printf("el id es : %d\n", this->id);
//Setsockopt -> opcional. 


}


Socket::~Socket(){
    Close();
}


void Socket::Close(){													//ESTE

}

/*
   char * hostip: direccion del servidor, por ejemplo "www.ecci.ucr.ac.cr"
   int port: ubicacion del proceso, por ejemplo 80
 */
int Socket::Connect( char * hostip, int port ) {						//ESTE
	
	int status; 	
	/*
	int status_aton = inet_aton(hostip, &my_socket.sin_addr); 				//convierte la direccion IP, a la forma binaria y lo guarda en el segundo parametro (esa estructura). 
	if (status_aton == 0) {
		perror("something went wrong");
	}
	*/
	
	//address.sin_addr.s_addr = INADDR_ANY; permite conexiones con cualquier dirección IP.  


	if (!this->ipv6) {
		struct sockaddr_in my_socket;
		my_socket.sin_addr.s_addr = inet_addr(hostip);						//inet_addr toma el string y lo convierte en direccion IP seguido por pnts. 
		my_socket.sin_port = htons(port); 		//convierte el integer del orden de bytes de host, al orden de bytes de red. 
		my_socket.sin_family = AF_INET;				//es ipv4 
			
		int status = connect(this->id, (struct sockaddr*)&my_socket, sizeof(my_socket));		//conecta la id del socket con la struct con los datos. 
			//si el id del socket es de datagrama, addr es la direccion del datagrama por default. y la otra direccion de donde es recibido 
			//si es de stream conecta, de nuevo el id con el addr, y ya esta. 
		
		if (-1 == status) {	
			perror("there was an error trying to connect the socket to the sockaddr"); 
		}		
	}
	else {
		struct sockaddr_in6 my_socket_ipv6;
		my_socket_ipv6.sin6_family = AF_INET6;
		inet_pton(AF_INET6, hostip, &my_socket_ipv6.sin6_addr);	//convierte el segundo parametro en una estrucutra de red del tipo(1er parametro) y lo deja en el tercer parametro. 
		my_socket_ipv6.sin6_port = htons(port);
		
		int status = connect(this->id, (struct sockaddr*)&my_socket_ipv6, sizeof(my_socket_ipv6)); 
		
		if (-1 == status) {
			perror("there was an erro trying to connect to sockaddr_ipv6"); 
		}
		
		//hacer el connect para los casos de ipv6. 
		
	} 

   return status; 
}


/*
   char * hostip: direccion del servidor, por ejemplo "www.ecci.ucr.ac.cr"
   char * service: nombre del servicio que queremos acceder, por ejemplo "http"
 */
 

int Socket::Connect( char *host, char *service ) {						//ESTE ? 
	
	int port = 0; 
	
	if (strcmp(service, "http") == 0) {
		port = 80; 
	}
	else {
		perror("the service you wish is not possible");
		exit(0); 	
	}
	
	int status; 	
	/*
	int status_aton = inet_aton(hostip, &my_socket.sin_addr); 				//convierte la direccion IP, a la forma binaria y lo guarda en el segundo parametro (esa estructura). 
	if (status_aton == 0) {
		perror("something went wrong");
	}
	*/
	
	//address.sin_addr.s_addr = INADDR_ANY; permite conexiones con cualquier dirección IP.  


	if (!this->ipv6) {
		struct sockaddr_in my_socket;
		my_socket.sin_addr.s_addr = inet_addr(host);						//inet_addr toma el string y lo convierte en direccion IP seguido por pnts. 
		my_socket.sin_port = htons(port); 		//convierte el integer del orden de bytes de host, al orden de bytes de red. 
		my_socket.sin_family = AF_INET;				//es ipv4 
			
		int status = connect(this->id, (struct sockaddr*)&my_socket, sizeof(my_socket));		//conecta la id del socket con la struct con los datos. 
			//si el id del socket es de datagrama, addr es la direccion del datagrama por default. y la otra direccion de donde es recibido 
			//si es de stream conecta, de nuevo el id con el addr, y ya esta. 
		
		if (-1 == status) {	
			perror("there was an error trying to connect the socket to the sockaddr"); 
		}		
	}
	else {
		struct sockaddr_in6 my_socket_ipv6;
		my_socket_ipv6.sin6_family = AF_INET6;
		inet_pton(AF_INET6, host, &my_socket_ipv6.sin6_addr);	//convierte el segundo parametro en una estrucutra de red del tipo(1er parametro) y lo deja en el tercer parametro. 
		my_socket_ipv6.sin6_port = htons(port);
		
		int status = connect(this->id, (struct sockaddr*)&my_socket_ipv6, sizeof(my_socket_ipv6)); 
		
		if (-1 == status) {
			perror("there was an erro trying to connect to sockaddr_ipv6"); 
		}
		
		//hacer el connect para los casos de ipv6. 
		
	} 

   return status; 
	
}



int Socket::Read( char *text, int len, int id_socket) {								
	 
	 int status; 
	 
	//soy el server y quiero hacerle read de lo que me manda el cliente. 
	status = read(id_socket, text, len);			

	if (-1 == status) {
		perror("read error"); 
	}


   return status; 

}


int Socket::Read(char* text, int len) {
	
	//soy el cliente y quiero hacerle read a lo que me manda el server. 
	int status = read(this->id, text, len);

	if (-1 == status) {
		perror("read error"); 
	}

	
	return status;  
}




int Socket::Write( char *text, int id_socket) {									

	int status = write(id_socket, text, strlen(text)); 	

	int close_status = close(id_socket);
	if (close_status == -1) {
		perror("there was an error trying to close the client after sending response"); 
	}
	else{
		printf("Connection closed\n");
	}
		
	if (status == -1) {
		perror("write error");
	}	

	return status; 
}


int Socket::Write(char* text) {
	
	int status = write(this->id, text, strlen(text)); 
	
	if (-1 == status) {
		perror("write error"); 
	}
	return status; 
}



int Socket::Listen( int queue ) {

	int status = listen(this->id, queue);
	if (-1 == status) {
		perror("error listening");
		close(this->id); 	
		return EXIT_FAILURE; 
	} 
	
    return status;
}



int Socket::Bind( int port, int server_client) {


	int status = 0; 
	int status_bind = 0; 
	int flag = 0; 
	
	if (0 == server_client) {			//para ipv4
			
			struct sockaddr_in server_socket; 
			memset(&server_socket, 0, sizeof(server_socket)); 
			server_socket.sin_family = AF_INET;
			server_socket.sin_addr.s_addr = htonl(INADDR_ANY); 		// inet_addr();	
			server_socket.sin_port = htons(port); 
			
			flag = 1; 
			
			status = setsockopt(this->id, SOL_SOCKET, SO_REUSEADDR, &flag, sizeof(flag));	
			if(-1 == status) {
				perror("setsockopt error"); 
				return EXIT_FAILURE; 
			}


			status_bind = bind(this->id, (struct sockaddr*)&server_socket, sizeof(server_socket)); 
			if (-1 == status_bind) {
				perror("there was an error trying to bind");
				close(this->id);			//no se realmente para que esto, o que hace. 
				return EXIT_FAILURE;  
			}
			
	}
	else {							//para ipv6
		
		struct sockaddr_in6 server_socket_ipv6;
		server_socket_ipv6.sin6_family = AF_INET6;
		server_socket_ipv6.sin6_addr = in6addr_any;					//inet_pton(AF_INET6, host, &my_socket_ipv6.sin6_addr);	 nose la diferencia. 
		server_socket_ipv6.sin6_port = htons(port);
		
		flag = 1; 
		
		
			status = setsockopt(this->id, SOL_SOCKET, SO_REUSEADDR, &flag, sizeof(flag));	
			if(-1 == status) {
				perror("setsockopt error"); 
				return EXIT_FAILURE; 
			}		
		

			status_bind = bind(this->id, (struct sockaddr*)&server_socket_ipv6, sizeof(server_socket_ipv6)); 
			if (-1 == status_bind) {
				perror("there was an error trying to bind");
				close(this->id);			//no se realmente para que esto, o que hace. 
				return EXIT_FAILURE;  
			}			
		
	}

    return status_bind;

}




int Socket::Accept(struct sockaddr_in* client_socket) {
		
	char addrs_client[INET_ADDRSTRLEN];
	
	socklen_t length_socket_client = sizeof(*client_socket); 
	
	int client_id = accept(this->id, (struct sockaddr*)client_socket, &length_socket_client); 
	if(-1 == client_id) {
		perror("accept error");
		close(this->id);
		return EXIT_FAILURE;   
	}
	else {
		inet_ntop(AF_INET, &(client_socket->sin_addr), addrs_client, sizeof(addrs_client));
		printf("New connection from: %s:%d ... \n", addrs_client, ntohs(client_socket->sin_port)); 	
	}
	
	this->client_id = client_id;
	
	return client_id; 	
}


int Socket::Accept(struct sockaddr_in6* client_socket_ipv6) {


	char addrs_client[INET6_ADDRSTRLEN];
	
	socklen_t length_socket_client = sizeof(*client_socket_ipv6); 
	
	int client_id = accept(this->id, (struct sockaddr*)client_socket_ipv6, &length_socket_client); 
	if(-1 == client_id) {
		perror("accept error");
		close(this->id);
		return EXIT_FAILURE;   
	}
	else {
		inet_ntop(AF_INET6, &(client_socket_ipv6->sin6_addr), addrs_client, sizeof(addrs_client));
		printf("New connection from: %s:%d ... \n", addrs_client, ntohs(client_socket_ipv6->sin6_port)); 	
	}
	
	this->client_id = client_id;
	
	return client_id; 		
	
}


/*

sockaddr_in  Socket::Accept(){

	struct sockaddr_in client_socket; 

	char addrs_client[INET_ADDRSTRLEN];
	
	int client_id = accept(this->id, (struct sockaddr*)&client_socket, sizeof(client_socket)); 
	if(-1 == client_id) {
		perror("accept error");
		close(this->id);

	}
	else {
		//inet_ntop(AF_INET, &(client_socket.sin_addr), addrs_client, sizeof(addrs_client));
		//printf("New connection from: %s:%d ... \n", addrs_client, ntohs(client_socket.sin_port)); 	
	}
	
	this->client_id = client_id;
	
	return client_socket; 
}

*/


int Socket::Shutdown( int mode ) {

    return -1;

}


void Socket::SetIDSocket(int id){

    this->id = id;

}


int Socket::Close(int server_client) {
	
	int close_status = -1;
	 
	if (0 == server_client) {
		//pendiente. 
	}
	else {
		close_status = close(this->client_id);
		if (-1 == close_status) {
			perror("close()"); 
		} 
	}
	
	printf("Connection closed\n");
	return close_status; 
}

