
#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include "server.h"


Server::Server(char type ,int ipv4_ipv6) {		//le paso el tipo, si es de stream o datagrama y si es ipv4 ó ipv6
	this->type_server = ipv4_ipv6;
	
	//this->msg_from_client = (char*)calloc(512, sizeof(char)); 
	
	this->server_socket = new Socket(type, ipv4_ipv6); 
	
	
	/*
	if (0 == ipv4_ipv6) {			//ipv4
		this->socket_client = (struct sockaddr_in*)calloc(1,sizeof(struct sockaddr_in)); 
	}
	else {						//ipv6
		this->socket_client6 = (struct sockaddr_in6*)calloc(1, sizeof(struct sockaddr_in6)); 
	}
	*/
}



void Server::bind(int port, int ipv4_ipv6) {
	
		if (-1 == this->server_socket->Bind(port, ipv4_ipv6)) {
			perror("there was an error trying to bind"); 
		}
		
}


void Server::listen(int length_queue) {
	
	if (-1 == this->server_socket->Listen(length_queue)) {
		perror("there was an error trying to listen"); 
	}
		
}


int Server::accept(struct sockaddr_in6* socket_client6) {

		int client_id = this->server_socket->Accept(socket_client6); 
		if (-1 == client_id) {
			perror("error on accept"); 
		}
		return client_id; 
}



int Server::accept(struct sockaddr_in* socket_client) {

	int client_id = 0; 

	client_id = this->server_socket->Accept(socket_client); 
	
	if (-1 == client_id) {
		perror("error on accept"); 
	}	
	
	return client_id; 
	//this->client_id = client_id; 
}


void Server::read(char* buf, int id_socket) {
	if (-1 == this->server_socket->Read(buf, 512, id_socket)) {
		perror("there was an error trying to read"); 
	}
}


void Server::write(char* buf, int client_id) {
	char* final_answer = (char*)calloc(512, sizeof(char));
	strcpy(final_answer, this->answer_to_client.c_str());  		//en answer_to_client ya hay 1 mensaje, al cual se le puede concatenar cosas, como el char* buf
																//de momento no hace nada. 
	this->server_socket->Write(final_answer, client_id);
	
	free(final_answer);  
}

