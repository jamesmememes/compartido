/*
 *  Establece la definicion de la clase Socket para efectuar la comunicacion
 *  de procesos que no comparten memoria, utilizando un esquema de memoria
 *  distribuida.  El desarrollo de la clase se hara en dos etapas, primero
 *  los metodos necesarios para los clientes, en la otra etapa los otros
 *  metodos para los servidores
 */

#ifndef Socket_h
#define Socket_h

class Socket{

    public:
        Socket( char, bool = false );
        Socket( int );
        ~Socket();
        int Connect( char *, int );
        int Connect( char *, char * );
        void Close();
        int Read( char *, int len,  int id_socket);	//este id puede ser el del server o del cliente, de acuerdo al parametro 3. 
        int Read(char*, int len); 
        int Write( char *, int id_socket);		//para el server escribirle al cliente. 
        int Write(char*); 						//para el cliente escribirle al server. 
        int Listen( int );
        int Bind( int, int);
        Socket* Accept();
        int Accept(struct sockaddr_in* client_socket);		//me retorna el client_id 
        int Accept(struct sockaddr_in6* client_socket_ipv6); 
        int Shutdown( int );
        void SetIDSocket( int );
        int Close(int ); 
        
    private:
        int id;
        bool ipv6;
        //esto es solo para el servidor. 
        int client_id; 
};

#endif

