#include "server.h"

#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>

#include <pthread.h>
#include <queue>

#define SERVER_PORT 7002

#define LENGTH_QUEUE 10

typedef struct {
char* msg_from_client;
std::string answer_to_client;
char* final_answer;
//Socket *server_socket;
Server* server; 
int client_id;

}thread_data_t;


void* run(void*data) {
	
		thread_data_t* thread_data = (thread_data_t*)data;  
	
		thread_data->server->read(thread_data->msg_from_client, thread_data->client_id); 
		
		printf("el mensaje recibido corresponde a : \n %s", thread_data->msg_from_client); 
		
		thread_data->server->write(thread_data->msg_from_client, thread_data->client_id); 	
}


int main (int argc, char*argv[]) {
	
	Server server('s', 0); 			//0-ipv4    1-ipv6
	
	server.bind(SERVER_PORT, 0); 	//0-ipv4     1-ipv6
	
	server.listen(LENGTH_QUEUE); 
	
	while(1) {


		struct sockaddr_in socket_client; 
		
		int client_id = server.accept(&socket_client); 

		thread_data_t* thread_data = (thread_data_t*)calloc(1,sizeof(thread_data_t)); //información para el hijo, para que atienda al cliente.
		thread_data->msg_from_client = (char*)calloc(512, sizeof(char));
		thread_data->answer_to_client = "mensaje recibido";
		thread_data->server = &server; 
		thread_data->client_id = client_id;		
		
		
		pthread_t* thread = (pthread_t*)malloc(1*sizeof(pthread_t)); //se declara 1 thread.
		//tengo que meter este thread en 1 cola, para luego poder destruirlo. pendiente.

		pthread_create(thread, NULL, run, thread_data); //para crear el thread, se ocupa, 1 subrutina(run) y 1 tipo de dato void*(thread_data).
			
	}
	return 0; 
}
