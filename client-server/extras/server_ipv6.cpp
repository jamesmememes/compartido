#include <stdio.h>
#include <stdlib.h>

#include <string>
#include <string.h> 

#include "socket.h"

#include <arpa/inet.h>

#define SERVER_PORT 7002


int main(int argc, char* argv[]) {
	
	std::string answer_to_client; 
	char* final_answer = (char*)calloc(512, sizeof(char)); 
	
	char* msg_from_client = (char*)calloc(512, sizeof(char)); 
	
	Socket server_socket('s', true);
	
	if(-1 == server_socket.Bind(SERVER_PORT, 1)){			// 0 de ipv4
		perror("there was an error");
	}

	
	if(-1 == server_socket.Listen(10)) {			//quiero tener 1 cola de 10 clientes que escuchar. 
		perror("there was an error"); 	
	} 
	 
	
	struct sockaddr_in6 socket_client;
	
		
	int client_id = server_socket.Accept(&socket_client);
	
	if(-1 == server_socket.Read(msg_from_client,512,1)) {
		perror("there was an error"); 	
	}
	else {
		//tengo el mensaje en msg_from_client. 
		printf("soy el server y recibi 1 mensaje : \n %s", msg_from_client); 
	}
	
	answer_to_client = "mensaje recibido"; 
	
	strcpy(final_answer, answer_to_client.c_str()); 
	
	server_socket.Write(final_answer, 1);		//respondale eso al (1)cliente.  
	
	
	server_socket.Close(1); 
	
	
	return 0; 
}

