#include <stdio.h>
#include <stdlib.h>

#include <string>
#include <string.h>

#include "socket.h"

#include <pthread.h>
#include <queue>

#include <arpa/inet.h>

#define SERVER_PORT 7002



typedef struct {
//aqui hay mucha mierda, realmente lo importante, es el client_id y el server_socket. lo demas son cosas x, que puse.
char* msg_from_client;
std::string answer_to_client;
char* final_answer;
Socket *server_socket;
int client_id;

}thread_data_t;


void* run(void* data) { //la subrutina que se le manda por parametro al pthread, debe tener 1 void* de parametro.

thread_data_t* thread_data = (thread_data_t*)data; //dado que la subrituna tiene que recibir 1 void*, se castea a lo que realmente es.


if(-1 == thread_data->server_socket->Read(thread_data->msg_from_client,512,thread_data->client_id)) {
perror("there was an error");
}
else {
//tengo el mensaje en msg_from_client.
printf("soy el server y recibi 1 mensaje : \n %s", thread_data->msg_from_client);
}

thread_data->final_answer = (char*)calloc(512, sizeof(char)); 

//a answer_to_client se le pueden concatenar cosas, para luego convertirlo a char y mandarlo, de momento solo manda el "mensaje recibido". 

strcpy(thread_data->final_answer, thread_data->answer_to_client.c_str());		


thread_data->server_socket->Write(thread_data->final_answer, thread_data->client_id); //respondale eso al (1)cliente.  
return NULL;
}



int main(int argc, char* argv[]) {
	
Socket server_socket('s', false);

if(-1 == server_socket.Bind(SERVER_PORT, 0)){ // 0 de ipv4
perror("there was an error");
}


if(-1 == server_socket.Listen(10)) { //quiero tener 1 cola de 10 clientes que escuchar.
perror("there was an error");
}



while(1) {

	struct sockaddr_in socket_client;


	int client_id = server_socket.Accept(&socket_client);

	if (-1 == client_id) {
		perror("something went wrong");
	}
	else {
		//tenemos que crear una struct, se llama thread_data, donde le vamos a mandar al hijo información.
		thread_data_t* thread_data = (thread_data_t*)calloc(1,sizeof(thread_data_t)); //información para el hijo, para que atienda al cliente.
		thread_data->msg_from_client = (char*)calloc(512, sizeof(char));
		thread_data->answer_to_client = "mensaje recibido";
		thread_data->server_socket = &server_socket;
		thread_data->client_id = client_id;
		//tengo que meter esta struct en 1 cola, para luego poder destruirla. pendiente.  

		pthread_t* thread = (pthread_t*)malloc(1*sizeof(pthread_t)); //se declara 1 thread.
		//tengo que meter este thread en 1 cola, para luego poder destruirlo. pendiente.

		pthread_create(thread, NULL, run, thread_data); //para crear el thread, se ocupa, 1 subrutina(run) y 1 tipo de dato void*(thread_data).
	}
}

server_socket.Close(1);


return 0;
}
