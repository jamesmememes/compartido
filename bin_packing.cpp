#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <list> 
#include <iterator> 
using namespace std; 

//#define V_2 true				//EXISTEN DOS VERSIONES
#define V_1 true				//USAR CUALQUIERA DE LAS 2. 


#ifdef V_1
//----------MEJOR SOLUCION--------------------------------
//globales para facilitar los llamados recursivos. 
std::list<list<int>>best_solution(0,list<int>()); 
int best_servers_amount = 0; 
int*servers_capacity; 
int solutions = 0;
//---------------------------------------------------------
#endif


#ifdef V_2
//----------MEJOR SOLUCION--------------------------------
//globales para facilitar los llamados recursivos. 
int**best_solution; 
int best_servers_amount = 0; 
int* servers_capacity;
int solutions = 0;  
//---------------------------------------------------------
#endif



void copy_data_lists() {
		
}


void copy_data_matrix() {
		
}

void showlist(list <int> g) 
{ 
    list <int> :: iterator it; 
    for(it = g.begin(); it != g.end(); ++it) 
       printf("%d  ", *it); 
    printf("\n");  
} 



void append_to_first_solution(int index_server, int virtual_machine_weight) {
	
	#if 1
	if (best_solution.size() < index_server+1) { 
		best_solution.push_back(std::list<int>(0,0)); 
	}
	
	std::list<list<int>>::iterator c_list = best_solution.begin(); 
	for (int index_list = 0; index_list < index_server; ++index_list) {
		++c_list; //iterator no tiene sobrecargado el operador "+", solo con el for sirve. 
	}
	
	c_list->push_back(virtual_machine_weight); 
	#endif
}


int first_fit(int virtual_machines, int* weights, int server_capacity) {
	int servers_needed = 1;
	
	std::list<int> servers_current_capacity(1, server_capacity); 
	std::list <int> :: iterator it; 
	
	it = servers_current_capacity.begin(); 	
	
	int current_virtual_machine_weight; 
	bool found_space = false; 
	int index_server = 0; 
	
	for (int virtual_machine = 0; virtual_machine < virtual_machines; ++virtual_machine) {
		current_virtual_machine_weight = weights[virtual_machine]; 
		
		index_server = 0; 
		it = servers_current_capacity.begin(); 
		while (index_server < servers_needed && !found_space) {
			if (current_virtual_machine_weight <= *it) {
				*it = *it - current_virtual_machine_weight;
				found_space = true;
				--index_server; 
			}
		++index_server; 
		++it; 
		}
		
		append_to_first_solution(index_server, current_virtual_machine_weight); 
		
		if (!found_space) {
			servers_needed++; 
			servers_current_capacity.push_back(server_capacity-current_virtual_machine_weight); 		
		}
		else {
			//no tengo que hacer nada. 
		}
		found_space = false;
	}
 
	return servers_needed; 

}



int servers_in_use(int* free_servers, int servers) {
	
	int counter_servers_in_use = 0; 
	
	for (int server = 0; server < servers; ++server) {
		if (free_servers[server] > 0) {
			++counter_servers_in_use; 
		}
	}
	return counter_servers_in_use; 
}


void print_list(std::list<list<int>>::iterator list)
{	
	std::list<int>::iterator it; 
	for(it = list->begin(); it != list->end(); ++it) {
       printf("%d  ", *it); 
	}
    printf("\n");  
	
}

void print_list_of_list(std::list<list<int>>*current_solution, int servers_needed, int* free_servers) {
	std::list<list<int>>::iterator c_list = current_solution->begin(); 

	int server = 0; 
	while (c_list != current_solution->end()) {
		if (free_servers[server] > 0) {
			printf("server : %d\n", server); 
			print_list(c_list); 
		}
		++c_list; 		
		++server; 
	}
}


void find_best_solution(int virtual_machine, int virtual_machines, int* weights,  std::list<list<int>>*current_solution, int* current_servers_capacity, int servers_needed, int* free_servers) {
	
	if (virtual_machine < virtual_machines) {	//puedo realizar un maximo de "virtual_machines" llamados recursivos. 
		std::list<list<int>>::iterator it = current_solution->begin(); 
		int server = 0; 
		while (server < servers_needed && servers_in_use(free_servers, servers_needed) <= best_servers_amount) { // <= para dar con todas las soluciones / < estricto para solo la mejor.
			int server_capacity = current_servers_capacity[server]; 
			int current_machine_weight = weights[virtual_machine]; 
			if (current_machine_weight <= server_capacity) {	//si lo puedo colocar en ese servidor. 
				++free_servers[server]; 
				current_servers_capacity[server] = current_servers_capacity[server] - current_machine_weight; 
				it->push_back(current_machine_weight); 
				find_best_solution(virtual_machine+1, virtual_machines, weights, current_solution, current_servers_capacity, servers_needed, free_servers);	//vuelve la historia a empezar.
				//me arrepiento ----- 
				--free_servers[server]; 
				current_servers_capacity[server]+= current_machine_weight;
				it->pop_back();
				//-------------------
			}
		++it; 
		++server; 
		}
	}
	else {//condicion de parada, ya asigné todas las maquinas virtuales. 	
		if (servers_in_use(free_servers, servers_needed) <= best_servers_amount) { //si la solucion encontrada es la mejor hasta ese momento encontrada.
			best_servers_amount = servers_in_use(free_servers, servers_needed); 
			//copy_data_lists();	
			printf("encontre una solucion con una cantidad de : %d servidores \n", servers_in_use(free_servers, servers_needed) );			
			print_list_of_list(current_solution, servers_needed, free_servers); 		
		}
		 
	}
	
	
}



void print_matrix(int** current_solution, int servers_needed, int* free_servers) {
	
	printf("------------------------------------------------------\n\n"); 
	for (int i = 0; i < servers_needed; ++i) {
		if (free_servers[i] > 0) {
			printf("server : %d\n", i); 
			for (int j = 0; j < 10; ++j) {
				printf("%d ", current_solution[i][j]); 
			}
			printf("\n"); 
		}
	}
	printf("-------------------------------------------------------\n\n");
}



void find_best_solution2(int virtual_machine, int virtual_machines, int* weights,  int**current_solution, int* current_servers_capacity, int servers_needed, int* free_servers, int columns) {
	
	if (virtual_machine < virtual_machines) {	//puedo realizar un maximo de "virtual_machines" llamados recursivos. 
		int server_column = 0; 
		int server = 0; 
		while (server < servers_needed && servers_in_use(free_servers, servers_needed) <= best_servers_amount) {	//por todos los posibles servers en los que se podria colocar. 
			int server_capacity = current_servers_capacity[server]; 
			int current_machine_weight = weights[virtual_machine]; 
			if (current_machine_weight <= server_capacity) {	//si lo puedo colocar en ese servidor. 
				++free_servers[server]; 
				current_servers_capacity[server] = current_servers_capacity[server] - current_machine_weight; 
				 current_solution[server_column][current_solution[server_column][columns]] = current_machine_weight; 
				 ++current_solution[server_column][columns];
				find_best_solution2(virtual_machine+1, virtual_machines, weights, current_solution, current_servers_capacity, servers_needed, free_servers,columns);	//vuelve la historia a empezar.
				 current_solution[server_column][current_solution[server_column][columns]] = 0; 
				 --current_solution[server_column][columns];				
				--free_servers[server]; 
				current_servers_capacity[server]+= current_machine_weight;
			}
		++server_column; 
		++server; 
		}
	}
	else {//condicion de parada, ya asigne todas las maquinas virtuales. 	
		if (servers_in_use(free_servers, servers_needed) <= best_servers_amount) { //al final debe ser menor estricto. 
			best_servers_amount = servers_in_use(free_servers, servers_needed); 
			//copy_data_matrix();
			printf("\nencontre una solucion con una cantidad de : %d servidores \n", servers_in_use(free_servers, servers_needed) );
			print_matrix(current_solution, servers_needed, free_servers); 
		}
		 
	}
	
	
}






int main (int argc, char* argv[]) {

/* 
 * pd: la cantidad de parametros ingresados, depende del parametro inicial, cantidad de maquinas virtuales. 
 * pd : referirse a linea 31,32
 * Parametros 
 * 1 : cantidad de maquinas virtuales
 * 2 - (N+1/con N+1 = cantidad de maquinas del parametro 1) : los pesos de cada una de las maquinas virtuales. 
 * N+2 : la capacidad de los servidores. 
 * example : ./run 5 6 7 2 9 4 15
 * 
 * */

int virtual_machines = atoi(argv[1]); 
assert(argc == virtual_machines+3);
assert(virtual_machines > 0); 

int * weights = (int*)calloc(virtual_machines, sizeof(int)); 


int offset = 2; 

for (int virtual_machine = 0; virtual_machine < virtual_machines; ++virtual_machine) {
	weights[virtual_machine] = atoi(argv[offset+virtual_machine]); 
}

int server_capacity = atoi(argv[offset+virtual_machines]); 


int servers_needed = first_fit(virtual_machines, weights, server_capacity); 


#if 0
printf("----------------------------------------------------------\n"); 
std::list<list<int>>::iterator uy_list = best_solution.begin(); 
std::list<int>::iterator uy; 
int size_m = best_solution.size(); 
int size_l = 0; 
for (int i = 0; i < size_m; ++i) {
	uy = uy_list->begin(); 	
	size_l = uy_list->size(); 
	for (int j = 0; j < size_l; ++j) {
		printf("%d ", *uy); 
		++uy; 
	}
	printf("\n"); 
	++uy_list; 
}
printf("-------------------------------------------------------\n"); 
#endif


#if 0
//corresponde a la mejor solucion. 
for (int index_server = 0; index_server < servers_needed; ++index_server) {
	best_solution.push_back(std::list<int>(0,0)); 
}

servers_capacity = (int*)malloc(servers_needed*sizeof(int)); 

//---------------------------
#endif


int* current_servers_capacity = (int*)malloc(servers_needed*sizeof(int)); 
for (int index_server = 0; index_server < servers_needed; ++index_server) {
	current_servers_capacity[index_server] = server_capacity; 
}

int* free_servers = (int*)calloc(servers_needed, sizeof(int)); 
best_servers_amount = servers_needed; 


#if 1
	#ifdef V_1
	std::list<list<int>>current_solution(servers_needed, list<int>()); 
	find_best_solution(0, virtual_machines, weights, &current_solution, current_servers_capacity, servers_needed, free_servers);	//el 1 final, al menos se necesita 1 servidor.  
	#endif


	#ifdef V_2
	int columns = 10; 
	int** current_solution = (int**)calloc(servers_needed, sizeof(int*)); 
	for (int s = 0; s < servers_needed; ++s) {
		current_solution[s] = (int*)calloc(columns, sizeof(int)); 
	}
	find_best_solution2(0, virtual_machines, weights, current_solution, current_servers_capacity, servers_needed, free_servers,columns-1);
	#endif
#endif

}

